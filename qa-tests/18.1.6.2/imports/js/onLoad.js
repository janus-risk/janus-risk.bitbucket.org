window.addEventListener("load", function(){

    var breadCrumbs = document.getElementsByClassName("breadCrumbs")[0];
    if(!breadCrumbs){
        return;
    }
    var pageUrl = document.URL;
    var allLinks = breadCrumbs.getElementsByTagName("a");
    if(!allLinks){
        return;
    }
    var a = allLinks[allLinks.length -1];
    if(a.href === pageUrl){
        a.style.display = "none";
        var nextS = a.nextSibling;
        nextS.textContent = '';
    }

});